package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.LinkedList;


public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private int position;
    private boolean fakeStatement = false;

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (statement == null || statement.isEmpty())
            return null;

        statement = '(' + statement.trim() + ')';

        LinkedList<Double> Operands = new LinkedList<>();
        LinkedList<Character> Operators = new LinkedList<>();

        position = 0;
        Object element;
        Object prevElement = 'X';

        do {
            element = getElement(statement);

            if (fakeStatement) {
                return null;
            }

            if (element instanceof Character && prevElement instanceof Character && (Character) prevElement == '(' && ((Character) element == '+' || (char) element == '-')) {
                Operands.push(0.0);
            }

            if (element instanceof Double) {
                Operands.push((Double) element);
            } else if (element instanceof Character) {
                if ((!isOperator((Character) element) && (Character) element != '(' && (Character) element != ')'))
                    return null;
                if (prevElement instanceof Character && isOperator((Character) prevElement) && isOperator((Character) element))
                    return null;
                if ((Character) element == ')') {
                    while (Operators.size() > 0 && Operators.peek() != '(')
                        popOperator(Operands, Operators);

                    Operators.pop();
                } else {
                    while (canToPop((Character) element, Operators))
                        popOperator(Operands, Operators);
                    Operators.push((Character) element);
                }

            }
            prevElement = element;
        }
        while (element != null);

        if (Operands.size() > 1 || Operators.size() > 0)
            return null;

        return new DecimalFormat("#.####").format(Operands.pop()).replaceAll(",", ".");
    }

    private Object getElement(String statement) {
        if (position == statement.length())
            return null;
        if (Character.isDigit(statement.toCharArray()[position])) {
            String res = readDouble(statement);
            return res == null ? null : Double.parseDouble(res);
        } else
            return statement.toCharArray()[position++];

    }


    private String readDouble(String statement) {
        StringBuilder stringBuilder = new StringBuilder();
        int countDot = 0;
        while (position < statement.length() && (Character.isDigit(statement.toCharArray()[position]) || statement.toCharArray()[position] == '.')) {
            if (statement.toCharArray()[position] == '.')
                countDot++;
            if (countDot > 1) {
                fakeStatement = true;
                return null;
            }
            stringBuilder.append(statement.toCharArray()[position++]);
        }

        return stringBuilder.toString();
    }

    private void popOperator(LinkedList<Double> Operands, LinkedList<Character> Operators) {
        double y = Operands.pop();
        double x = Operands.pop();


        switch (Operators.pop()) {
            case '-':
                Operands.push(x - y);
                break;
            case '+':
                Operands.push(x + y);
                break;
            case '*':
                Operands.push(x * y);
                break;
            case '/':
                if (y == 0)
                    fakeStatement = true;
                Operands.push(x / y);
                break;

        }
    }

    private boolean canToPop(Character operation, LinkedList<Character> Operators) {
        if (Operators.size() == 0)
            return false;
        int p1 = getPriority(operation);
        int p2 = getPriority(Operators.peek());

        return p1 >= 0 && p2 >= 0 && p1 >= p2;
    }

    private int getPriority(Character operator) {
        switch (operator) {
            case '(':
                return -1;
            case '*':
            case '/':
                return 1;
            case '+':
            case '-':
                return 2;
            default:
                fakeStatement = true;
                return -2;
        }
    }

    private boolean isOperator(Character symbol) {
        return symbol == '-' || symbol == '+' || symbol == '/' || symbol == '*';
    }


}
