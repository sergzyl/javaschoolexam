package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null && y == null)
            return true;
        if (x == null || y == null)
            throw new IllegalArgumentException();

        List z = (List) y.stream().filter(x::contains).collect(Collectors.toList());

        if (z.size() == x.size())
            return z.equals(x);

        while (x.size() != 0) {
            if (x.size() > z.size())
                return false;
            if (x.get(0).equals(z.get(0))) {
                x.remove(0);
                z.remove(0);
            } else {
                z.remove(0);
            }

        }


        return true;
    }


}
