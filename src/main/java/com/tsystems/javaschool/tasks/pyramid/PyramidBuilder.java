package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int floors = 0;
        int numberOfBlocks = 0;
        boolean canBuild = false;

        if (inputNumbers.size() > 1000 || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        for (int i = 1; i < inputNumbers.size(); i++) {
            numberOfBlocks += i;
            floors++;
            if (numberOfBlocks == inputNumbers.size()) {
                canBuild = true;
                break;
            }
        }

        if (!canBuild)
            throw new CannotBuildPyramidException();

        int height = floors;
        int width = floors * 2 - 1;
        int k = inputNumbers.size() - 1;

        int [][] result = new int[height][width];
        for (int i = height - 1; i >= 0; i--) {
            int counter = (height - 1) - i;
            int numberOfZeros = counter;
            for (int j = width - 1; j >= 0; j--) {
                if ((j == width - 1 - counter) && (j >= numberOfZeros)) {
                    result[i][j] = inputNumbers.get(k--);
                    counter += 2;
                }
            }
        }

        return result;
    }
}
